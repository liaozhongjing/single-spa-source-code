import { appShouldBeActive, getAppChanges } from "../application/app.helpers.js";
import { toBootstrapPromise } from "../lifecycle/bootstrap.js";
import { toLoadPromise } from "../lifecycle/load.js";
import { toMountPromise } from "../lifecycle/mount.js";
import { toUnmountPromise } from "../lifecycle/unmount.js";
import { started } from "../start.js";
import { callCapturedEventListeners } from "./navigation-event.js";


export function reroute(event){
    //获取当前urlxia即将被加载、挂载、卸载的应用
    let {appsToLoad,appsToMount,appsToUnmount} = getAppChanges()
    // debugger
    if(started){
        //如果用户调用过start方法，就去处理应用的卸载和挂载
        return performAppChange()
    }

    //加载应用
    return loadApps()
    function loadApps(){
       return Promise.all(appsToLoad.map(toLoadPromise)).then(()=>callCapturedEventListeners(event))
    }

    function performAppChange(){
        // debugger
        //先将不需要的应用卸载
        const unmountPromise = Promise.all(appsToUnmount.map(toUnmountPromise))

        //加载未加载且需要的应用 =》 启动对应的应用 =》 挂载对应的应用
        const loadMountPromise = Promise.all(appsToLoad.map(app=>toLoadPromise(app).then(app=>{
            return tryBootstrapAndMount(app,unmountPromise)
        })))

        //启动和挂载需要挂载的应用
        const mountPromise = Promise.all(appsToMount.map(app=>tryBootstrapAndMount(app,unmountPromise)))

        //启动和挂载应用
        async function tryBootstrapAndMount(app,unmountPromise) {
            // debugger
            if(appShouldBeActive(app)){
                //保证卸载完成之后在加载
                // return toBootstrapPromise(app).then(app=>unmountPromise.then(()=>toMountPromise(app)))
                app = await toBootstrapPromise(app)
                await unmountPromise
                await toMountPromise(app)
            }
            return app
        }

        mountPromise.then(()=>{
            console.log('挂载完毕----------');
        })

        return Promise.all([loadMountPromise,mountPromise]).then(()=>{
            callCapturedEventListeners(event)
        })
    }

}