// 为了保证应用加载逻辑被最先执行，对路由的一系列方法进行重写劫持。确保加载应用的逻辑最先被调用，其次手动派发事件。
// 如果当前应用正在加载时，并且用户频繁切换路由，我们会将此时的reroute方法暂存起来，等待当前应用加载完毕后再次出发reroute渲染应用，从而节约性能。

import { reroute } from "./reroute.js"


function urlReroute(){
    reroute(arguments)
}

window.addEventListener("hashchange",urlReroute)
window.addEventListener("popstate",urlReroute)


//劫持原生事件，保证应用加载完后在切换路由

const captureEventListeners = {
    hashchange:[],
    popstate:[]
}

const listenerTo = ['hashchange','popstate']
const originAddEventListener = window.addEventListener
const originRemoveEventListener = window.removeEventListener

window.addEventListener = function(eventName,cb){
    if(listenerTo.includes(eventName) && !captureEventListeners[eventName].some(i=>i===cb)){
        return captureEventListeners[eventName].push(cb)
    }
    return originAddEventListener.apply(this,arguments)
}

window.removeEventListener = function(eventName,cb){
    if(listenerTo.includes(eventName)){
        return captureEventListeners[eventName] = captureEventListeners[eventName].filter(fn=>fn !== cb)
    }
    return originRemoveEventListener.apply(this,arguments)
}

function patchedUpdateState(updateState, methodName) {
    return function () {
        const urlBefore = window.location.href;
        const result = updateState.apply(this, arguments); // 调用原有pushState,replaceState方法
        const urlAfter = window.location.href;
        // pushState,replaceState时，也触发popstate
        if (urlBefore !== urlAfter) {
            const state = window.history.state;
            urlReroute(new PopStateEvent('popstate', { state }));
        }
        return result;
    }
}
// 加强pushState 和 repalceState方法，调用这两个方法本身不会触发popstate事件，用过重写的方式去触发urlReroute
window.history.pushState = patchedUpdateState(window.history.pushState, 'pushState');
window.history.replaceState = patchedUpdateState(window.history.replaceState, 'replaceState');

// 在子应用加载完毕后调用此方法，执行拦截的逻辑（保证子应用加载完后执行）
export function callCapturedEventListeners(eventArguments) {
    if (eventArguments) { // eventArguments => [{type: 'popstate'}, ...]
        const eventType = eventArguments[0].type; // popstate hashchange
        if (listenerTo.indexOf(eventType) >= 0) {
            // 调用保存的函数队列
            captureEventListeners[eventType].forEach((listener) => {
                listener.apply(this, eventArguments);
            });
        }
    }
}