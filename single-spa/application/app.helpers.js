/**
 * 子应用状态
 */

import { apps } from "./app.js"

//加载流程
export const NOT_LOADED = 'NOT_LOADED' //未加载

export const LOADING_SOURCE_CODE = 'LOADING_SOURCE_CODE'//加载中

export const LOAD_ERROR = 'LOAD_ERROR'//加载失败

//启动过程

export const NOT_BOOTSTRAPPED = 'NOT_BOOTSTRAPPED'//资源加载完成，还未启动

export const BOOTSTRAPPING = 'BOOTSTRAPPING'//启动中

export const NOT_MOUNTED = 'NOT_MOUNTED'//启动完成，还未挂载


// 挂载过程
export const MOUNTING = 'MOUNTING'//正在挂载

export const MOUNTED = 'MOUNTED'//挂载完成

export const UPDATING = 'UPDATING'//更新中  更新 ==》挂载

// 卸载流程
export const UNMOUNTING = 'UNMOUNTING'

// 以下三种状态这里没有涉及
export const UNLOADING = 'UNLOADING'//移除加载
export const SKIP_BECAUSE_BROKEN = 'SKIP_BECAUSE_BROKEN'//代码出错


//判断一个app是否处于激活状态（是否挂载）
export function isActive(app) {
    return app.status == MOUNTED
}

//判断一个app是否应该要被激活
export function appShouldBeActive(app) {
    return app && app.activeWhen(window.location)
}

////获取当前url下即将被加载、挂载、卸载的应用
export function getAppChanges() {
    let appsToLoad = []
    let appsToMount = []
    let appsToUnmount = []

    apps.forEach(app => {
        switch (app.status) {
            case NOT_LOADED:
            case LOAD_ERROR:
            case LOADING_SOURCE_CODE:
                appShouldBeActive(app) && appsToLoad.push(app)
                break;
            case NOT_BOOTSTRAPPED:
            case BOOTSTRAPPING:
            case NOT_MOUNTED:
                appShouldBeActive(app) && appsToMount.push(app)
                break;
            case MOUNTED:
                !appShouldBeActive(app) && appsToUnmount.push(app)
                break;

        }
    })

    return { appsToLoad, appsToMount, appsToUnmount }
}