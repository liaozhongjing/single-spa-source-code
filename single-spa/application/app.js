import { reroute } from "../navigation/reroute.js";
import {NOT_LOADED} from "./app.helpers.js"

export const apps = []  //存储所有注册的app

export function registerApplication(appName,loadApp,activeWhen,customProps){
    let app = {
        name:appName,
        loadApp,
        activeWhen,
        customProps,
        status:NOT_LOADED
    }

    apps.push(app)



    reroute();//重要
}