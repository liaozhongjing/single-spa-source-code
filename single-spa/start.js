import { reroute } from "./navigation/reroute.js"
import "./navigation/navigation-event.js"

export let started = false //标记用户是否调用过start方法
export function start(){
    started = true
    reroute()
}