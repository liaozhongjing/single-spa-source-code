import { BOOTSTRAPPING, NOT_BOOTSTRAPPED, NOT_MOUNTED } from "../application/app.helpers.js"

export function toBootstrapPromise(app){
    return new Promise((resolve,reject)=>{
        if(app.status !== NOT_BOOTSTRAPPED) return resolve(app)
        app.status = BOOTSTRAPPING
        app.bootstrap(app.customProps).then(()=>{
            app.status = NOT_MOUNTED
            resolve(app)
        })
    })
}