import { LOADING_SOURCE_CODE, NOT_BOOTSTRAPPED, NOT_LOADED } from "../application/app.helpers.js"


//用户传的生命周期用可能时个数组，也有可能时函数，该函数就是将用户传的生命周期转换成一个总的函数
//如果时数组，将按照顺序执行
function flattenArrayToPromise(fns){
    fns = Array.isArray(fns) ? fns : [fns]

    return function(prop){
        return fns.reduce((promise,fn)=>promise.then(()=>fn(prop)),Promise.resolve())
    }
}

export function toLoadPromise(app){
    return new Promise((resolve,reject)=>{
        if(app.status !== NOT_LOADED){
            return resolve(app)
        }
        app.status = LOADING_SOURCE_CODE //资源加载中
        app.loadApp(app.customProps).then(v=>{
            const {bootstrap,mount,unmount} = v //获取资源的生命周期
            app.bootstrap = flattenArrayToPromise(bootstrap)
            app.mount = flattenArrayToPromise(mount)
            app.unmount = flattenArrayToPromise(unmount)
            app.status = NOT_BOOTSTRAPPED
            resolve(app)
        })
    })
}