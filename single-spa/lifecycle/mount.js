import {  MOUNTED, MOUNTING, NOT_MOUNTED } from "../application/app.helpers.js"

export function toMountPromise(app){
    return new Promise((resolve,reject)=>{
        if(app.status !== NOT_MOUNTED) return resolve(app)
        app.status = MOUNTING
        app.mount(app.customProps).then(()=>{
            app.status = MOUNTED
            resolve(app)
        })
    })
}