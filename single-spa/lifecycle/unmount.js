import { MOUNTED, NOT_MOUNTED, UNMOUNTING } from "../application/app.helpers.js"

export function toUnmountPromise(app){
    return new Promise((resolve,reject)=>{
        if(app.status !== MOUNTED) return resolve(app)
        app.status = UNMOUNTING
        app.unmount(app.customProps).then(()=>{
            app.status = NOT_MOUNTED
            resolve(app)
        })
    })
}