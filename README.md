index.html文件需要通过使用本地服务器的方式打开


总结：整个single-spa就相当于是维护应用的状态机，每个子应用都有独立的状态，子应用需要暴露接入协议，bootstrap、mount、unmount等等生命周期函数，内部会监听url地址的变化从而去匹配我们的应用，执行对应的生命周期函数。

缺点：1.没有js和css沙箱隔离效果 2.微应用资源需要手动加载

1.registerApplication：主要时将注册的应用存储起来，
2.start：调用start之前，应用会被加载，但不会初始化、挂载和卸载，有了start可以更好的控制应用的性能
3.reroute：（重点）负责在地址栏变化时去修改应用的状态以及执行生命周期
4.监听url变化：监听hashchange和popstate事件，拦截removeEventListener、addEventListener方法中注册“hashchange”和“popstate”的事件进行暂存起来，等待当前应用加载完毕后再去手动触发这些事件，
重写增强window.history.pushStat和window.history.replaceState，因为这两个方法本身不会触发popstate事件


流程：

调用registerApplication后，并还没调用start时，这时会先预加载当前url下匹配的应用资源  =》 调用start方法后，才能进行应用的 初始化、挂载和卸载操作
=》 监听url变化，去调用reroute修改应用的状态，执行应用 暴露出来的声明周期函数


